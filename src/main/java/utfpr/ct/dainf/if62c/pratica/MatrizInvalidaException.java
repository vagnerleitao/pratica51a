/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;


/**
 *
 * @author vagner
 */
public class MatrizInvalidaException extends Exception {
    //Esta exceção indica que uma matriz tem uma ou ambas as dimensões
    //de tamanho menor ou igual a zero

    private final int m;
    private final int n;
    
    
    public MatrizInvalidaException(int m,int n) {
        //super(String.format("Matriz de %dx%d não pode ser criada.\n",m,n));
        
        this.m=m;
        this.n=n;
        m=getNumLinhas();
        n=getNumColunas();
        System.out.format("Matriz de %dx%d não pode ser criada.\n",m,n);
        //String msg = String.format("Matriz de %dx%d não pode ser criada.\n",m,n);
        
    }
    
    
    public int getNumLinhas(){ // Retorna número de linhas da matriz que se tentou criar
        //Matriz valor=new Matriz();
        //int m = valor.linhasM;
        //double[][] valor = new Matriz.getMatriz();
        return m;
    }
    
    public int getNumColunas() {  // Retorna número de colunas da matriz que se tentou criar
        //Matriz valor=new Matriz();
        //int n = valor.colunasM;
        return n;
    }
    
}
