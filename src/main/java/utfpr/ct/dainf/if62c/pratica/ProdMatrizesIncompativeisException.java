/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Arrays;

/**
 *
 * @author vagner
 */
public class ProdMatrizesIncompativeisException extends MatrizesIncompativeisException {

    public ProdMatrizesIncompativeisException(Matriz m1, Matriz m2) throws MatrizInvalidaException {
        super(m1, m2);
        int linhaM1 = m1.getMatriz().length;
        int colunaM1 = m1.getMatriz()[0].length;
        int linhaM2 = m2.getMatriz().length;
        int colunaM2 = m2.getMatriz()[0].length;
        System.out.format("Matrizes de %dx%d e %dx%d não podem ser multiplicadas.\n",
              linhaM1, colunaM1,
              linhaM2, colunaM2)
             ;
        //super(String.format("Matrizes de %dx%d e %dx%d incompatíveis para a operação",
        //    m1.getMatriz().length, m1.getMatriz()[0].length,
        //    m2.getMatriz().length, m2.getMatriz()[0].length));
        
    }
    
}
