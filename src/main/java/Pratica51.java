import java.util.Random;
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    private static final int linhas=3;
    private static final int colunas=2;
    private static final int linhasMS=3;
    private static final int colunasMS=2;
    private static final int linhasNS=3;
    private static final int colunasNS=2;
    private static final int linhasMP=4;
    private static final int colunasMP=5;
    private static final int linhasNP=5;
    private static final int colunasNP=3;
    private static Matriz M;
    private static Matriz N;
    private static Matriz matrizSoma;
    private static Matriz matrizProduto;
    private static double[][] m;
    private static double[][] n;
    private static double[][] gera;
    private static double[][] transfSoma;
    private static double[][] transfProduto;
    private static double[][] resultadoSoma;
    private static double[][] resultadoProduto;
    
    
    public static void main(String[] args) throws MatrizInvalidaException, SomaMatrizesIncompativeisException, ProdMatrizesIncompativeisException  {
        try{
        Matriz M=new Matriz(linhas,colunas);
        m = M.getMatriz();
        Random r = new Random();
        for(int i=0;i<linhas;i++){
            for(int j=0;j<colunas;j++){
                m[i][j]=r.nextDouble();
                m[i][j]=(double)Math.round(m[i][j]*1000)/1000;
            }
        }
        System.out.println("Matriz original: " + M);
        }
        catch (MatrizInvalidaException miex){ miex.getLocalizedMessage(); }

        try{
        Matriz M=new Matriz(linhasMS,colunasMS);
        m = M.getMatriz();
        Random r = new Random();
        for(int i=0;i<linhasMS;i++){
            for(int j=0;j<colunasMS;j++){
                m[i][j]=r.nextDouble();
                m[i][j]=(double)Math.round(m[i][j]*1000)/1000;
            }
        }
        Matriz N=new Matriz(linhasNS,colunasNS);
        n = N.getMatriz();
        for(int i=0;i<linhasNS;i++){
            for(int j=0;j<colunasNS;j++){
                n[i][j]=r.nextDouble();
                n[i][j]=(double)Math.round(n[i][j]*1000)/1000;
            }
        }
        matrizSoma = new Matriz(linhasMS,colunasMS);
        resultadoSoma=matrizSoma.getMatriz();  // m1 tranfSoma
        matrizSoma=N.soma(M);
        
        System.out.println("Matriz original 1 - Soma : " + M);
        System.out.println("Matriz original 2 - Soma : " + N);
        System.out.println("Matriz Soma da Original 1 e Original 2 : "+matrizSoma);
        }
        catch (MatrizInvalidaException miex){ miex.getLocalizedMessage(); }
        catch (SomaMatrizesIncompativeisException smiex) { smiex.getLocalizedMessage(); }
    
        try{
        Matriz M=new Matriz(linhasMP,colunasMP);
        m = M.getMatriz();        
        Random r = new Random();        
        for(int i=0;i<linhasMP;i++){
            for(int j=0;j<colunasMP;j++){
                m[i][j]=r.nextDouble();
                m[i][j]=(double)Math.round(m[i][j]*1000)/1000;
            }
        }
        Matriz N=new Matriz(linhasNP,colunasNP);
        n = N.getMatriz();
        for(int i=0;i<linhasNP;i++){
            for(int j=0;j<colunasNP;j++){
                n[i][j]=r.nextDouble();
                n[i][j]=(double)Math.round(n[i][j]*1000)/1000;
            }
        }
        matrizProduto = new Matriz(linhasMP,colunasMP);
        resultadoSoma=matrizProduto.getMatriz();  // m1 tranfSoma
        matrizProduto=N.prod(M);
        
        System.out.println("Matriz original 1 - Produto : " + M);
        System.out.println("Matriz original 2 - Produto : " + N);
        System.out.println("Matriz Produto da Original 1 e Original 2 : "+matrizProduto);
        }
        catch (MatrizInvalidaException miex){ miex.getLocalizedMessage(); }
        catch (ProdMatrizesIncompativeisException pmiex) { pmiex.getLocalizedMessage(); }
        
    }
        
}
